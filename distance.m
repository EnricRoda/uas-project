function [d] = distance(p_a, p_b)

x_a = p_a(1);
y_a = p_a(2);

x_b = p_b(1);
y_b = p_b(2);

d = sqrt((x_a - x_b)^2 + (y_a - y_b)^2);

end