function [sl, tau, dmod, zthr, alim, hmd] = alarm_thresholds(alt)

if 1000 <= alt && alt < 2350
    
    sl = 3;
    tau = 15;
    dmod = 0.20;
    zthr = 600;
    alim = 300;
    hmd = 1215;
    
elseif 2350 <= alt && alt < 5000
    
    sl = 4;
    tau = 20;
    dmod = 0.35;
    zthr = 600;
    alim = 300;
    hmd = 2126;
    
elseif 5000 <= alt && alt < 10000
    
    sl = 5;
    tau = 25;
    dmod = 0.55;
    zthr = 600;
    alim = 350;
    hmd = 3342;
    
elseif 10000 <= alt && alt < 20000
    
    sl = 6;
    tau = 30;
    dmod = 0.80;
    zthr = 600;
    alim = 400;
    hmd = 4861;
    
elseif 20000 <= alt && alt < 42000
    
    sl = 7;
    tau = 35;
    dmod = 1.10;
    zthr = 700;
    alim = 600;
    hmd = 6683;
    
elseif 42000 <= alt
    
    sl = 7;
    tau = 35;
    dmod = 1.10;
    zthr = 800;
    alim = 700;
    hmd = 6683;
    
end

end