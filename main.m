close all; clear; clc;

%% Initialize variables

d_sep = 5; % NM

d_h = 5; % deg
d_t = 1; % s

beta = 180; % deg

v_a = 500; % kts
v_b = 170; % kts
t_c = 2;   % min

%% Convert units to IS

d_sep = convlength(d_sep, 'naut mi', 'm'); % m

v_a = convvel(v_a, 'kts', 'm/s'); % m/s
v_b = convvel(v_b, 'kts', 'm/s'); % m/s
t_c = t_c * 60; % s

%% Simulation

for delta_h = 0:d_h:90 % deg
    
    i = 1;
    [p_a, p_b] = init_position(v_a, v_b, t_c, beta);
    d(i) = distance(p_a, p_b);
    
    i = i + 1;
    
    for t = 0:d_t:(2 * t_c)
        
        [p_a, p_b] = move_fw(p_a, p_b, v_a, v_b, t, beta, delta_h);
        d(i) = distance(p_a, p_b);
        
        i = i + 1;
        
    end
    
    if min(d) > d_sep
        delta_h_min = delta_h;
        d_min = min(d);
        
        fprintf('delta_h_min:\t%d\n', delta_h_min);
        fprintf('d_min:\t\t%d\n\n', d_min);
        break
    end
    
end

% Validation
figure; hold on; grid on;
plot(d_sep * ones(size(d)));
plot(d);


for bank = -25:1:25
    
    bank = bank*pi/180;
    i = 1;
    [p_a, p_b] = init_position(v_a, v_b, t_c, beta);
    d_bank(i) = distance(p_a, p_b);
    
    i = i + 1;
    
    for t = 0:d_t:(2 * t_c)
        
        d_bank(i) = bankedturn(p_a, p_b, v_a, v_b, beta, bank, t);
        
        i = i + 1;
        
    end
    
    if min(d_bank) > d_sep
        bank_min = bank;
        d_bank_min = min(d_bank);
        
        fprintf('bank_min:\t%d\n', bank*180/pi);
        fprintf('d_bank_min:\t%d\n\n', d_bank_min);
        break
    end
    
end

% Validation
figure; hold on; grid on;
plot(d_sep * ones(size(d_bank)));
plot(d_bank);

%% Resolution Advisory (RA)

v_a = [500, 0]; % kts
v_b = 170 * [cosd(beta), sind(beta)]; % kts

v_a = convvel(v_a, 'kts', 'm/s'); % m/s
v_b = convvel(v_b, 'kts', 'm/s'); % m/s

alt = 1500;
[sl, tau, dmod, zthr, alim, hmd] = alarm_thresholds(alt);
dmod = convlength(dmod, 'naut mi', 'm'); % m

x_a = 0 - v_a(1) * t_c;
y_a = 0 - v_a(2) * t_c;

x_b = 0 - v_b(1) * cosd(beta) * t_c;
y_b = 0 - v_b(2) * sind(beta) * t_c;

p_a = [x_a, y_a];
p_b = [x_b, y_b];

v_r = p_a - p_b;
v_r_dot = v_a - v_b;

tau_mod = -(distance(p_a, p_b)^2 - dmod^2)/dot(v_r, v_r_dot);
fprintf('tau_mod:\t%d\n', tau_mod);

