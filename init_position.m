function [p_a, p_b] = init_position(v_a, v_b, t_c, beta)

% We are always assuming that aircraft A travels along the x axis only.
% This function sets the aircraft at positons A and B, respectively.

x_a = 0 - v_a * t_c;
y_a = 0;

x_b = 0 - v_b * cosd(beta) * t_c;
y_b = 0 - v_b * sind(beta) * t_c;

p_a = [x_a, y_a];
p_b = [x_b, y_b];

end