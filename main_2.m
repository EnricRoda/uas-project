close all; clear; clc;

%% Initialize variables

v_a = 500; % kts
v_b = 170; % kts
delta_h = 45; % deg
d_sep = 5; % NM


%% Find t_min for all betas.

[t_min_instant, t_min_banked, beta] = find_t_min_all_betas(v_a, v_b, delta_h, d_sep);

beta_rad = deg2rad(beta);

h_1 = polarplot(beta_rad, t_min_instant, 'b-', 'DisplayName', 't_{min} instant [s]');
hold on;
h_2 = polarplot(beta_rad, t_min_banked, 'r-', 'DisplayName', 't_{min} banked [s]');
hold off;

title('t_{min} vs \beta');

legend('Location', 'southoutside', 'NumColumns', 2);

dim = [0, 0.7, 0.4, 0.3];
str = sprintf([
    'v_a: %d kts\n'...
    'v_b: %d kts\n'...
    'delta_h: %d deg\n'...
    'd_{sep}: %d NM\n'],...
    v_a, v_b, delta_h, d_sep);
annotation('textbox', dim, 'String', str, 'FitBoxToText', 'on');

% Zoom in polar plot.
ax = gca;
ax.RLim = [0, 40];