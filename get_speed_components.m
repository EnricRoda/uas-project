function [v_b] = get_speed_components(v_b, beta)

% We are always assuming that aircraft A travels along the x axis only.
% This allows us to work with the relative bearing (beta) between aircraft.
% This function recovers the x and y components of the speed vector from
% its scalar magnitude (norm).
%
% This function only applies to aircraft B-

v_b = v_b * [cosd(beta), sind(beta)];

end