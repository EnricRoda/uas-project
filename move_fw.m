function [p_a, p_b] = move_fw(p_a, p_b, v_a, v_b, t, beta, varargin)

% move_fw(p_a, p_b, v_a, v_b, t, beta)
% move_fw(p_a, p_b, v_a, v_b, t, beta, delta_h)

switch nargin - 6
    case 1
        delta_h = varargin{1};
        angle = beta - delta_h;
    otherwise
        angle = beta;
end

x_0_a = p_a(1);
y_0_a = p_a(2);

x_0_b = p_b(1);
y_0_b = p_b(2);

x_a = x_0_a + v_a * t;
y_a = y_0_a;

x_b = x_0_b + v_b * cosd(angle) * t;
y_b = y_0_b + v_b * sind(angle) * t;

p_a = [x_a, y_a];
p_b = [x_b, y_b];

end