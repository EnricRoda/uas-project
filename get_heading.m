function [h] = get_heading(varargin)

% get_heading(v)
% get_heading(p_0, p_1)

% % You can use the diff function to get all the differences at once:
% dxy = diff(xy); % will contain [xy(2,1)-xy(1,1) xy(2,2)-xy(1,2); ...
%
% % Then you compute the angle using the atan2 function:
% h = 90 - atan2d(dxy(:, 2), dxy(:, 1));
%
% % And finally take the angle modulo 360 to get it between 0 and 360:
% h = mod(h, 360);

hdg = @(a, b) mod(90 - atan2d(b, a), 360);

switch nargin
    
    case 1 % Heading from speed vector.
        if all(varargin{1} == 0)
            h = NaN;
        else
            h = hdg(varargin{1}(1), varargin{1}(2));
        end
        
    case 2
        if all(cellfun(@isscalar, varargin)) % Headring from scalar speed and beta.
            v_b = varargin{1};
            beta = varargin{2};
            v_b_comp = get_speed_components(v_b, beta);
            
            if all(v_b_comp == 0)
                h = NaN;
            else
                h = hdg(v_b_comp(1), v_b_comp(2));
            end
            
        else % Heading from 2 positions.
            xy = [varargin{1}; varargin{2}];
            dxy = diff(xy);
            if all(dxy == 0)
                h = NaN;
            else
                h = hdg(dxy(:, 1), dxy(:, 2));
            end
        end
        
    otherwise
        error('Bad input arguments.');
end

end