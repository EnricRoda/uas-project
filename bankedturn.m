function [dist, p_a, p_b] = bankedturn(p_a, p_b, v_a, v_b, beta, bank, t)

g = 9.81; % m/s²

if bank == 0
    
    [p_a, p_b] = move_fw(p_a, p_b, v_a, v_b, t, beta);
    
else
    
    radius = abs(v_b^2/(g * tan(bank))); % m
    omega = v_b/radius; % rad/s
    
    if bank < 0 % Left turn.
        phi_0 = beta - pi/2;
        x_oc = p_b(1, 1) - radius * cos(phi_0);
        y_oc = p_b(1, 2) - radius * sin(phi_0);
    else        % Right turn.
        phi_0 = pi/2 + beta;
        x_oc = p_b(1, 1) - radius * cos(phi_0);
        y_oc = p_b(1, 2) - radius * sin(phi_0);
    end
    
    if bank < 0 % Left turn.
        phi = phi_0 + omega * t;
    else        % Right turn.
        phi = phi_0 - omega * t;
    end
    
    p_a = [p_a(1) + v_a * t, 0];
    p_b = [x_oc + radius * cos(phi), y_oc + radius * sin(phi)];
    
end

dist = distance(p_a, p_b);

end