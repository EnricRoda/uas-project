function [p_a, p_b] = init_collision()

% We are always assuming that aircraft A travels along the x axis only.
% This function sets both aircraft at positon C (0, 0).

p_a = [0, 0];
p_b = p_a;

end