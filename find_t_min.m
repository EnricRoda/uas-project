function [t_min_instant, t_min_banked] = find_t_min(v_a, v_b, beta, delta_h, d_sep)
%% Initialize variables

% v_a = 500; % kts
% v_b = 170; % kts
%
% beta = 180; % deg
% delta_h = 90; % deg
%
% d_sep = 5; % NM
d_t = 1; % s


%% Convert units to IS

v_a = convvel(v_a, 'kts', 'm/s'); % m/s
v_b = convvel(v_b, 'kts', 'm/s'); % m/s

beta_rad = deg2rad(beta); % rad

d_sep = convlength(d_sep, 'naut mi', 'm'); % m


%% Instant Turn.

[p_a, p_b] = init_collision();

t = 0;
while distance(p_a, p_b) < d_sep
    [p_a, p_b] = move_bw(p_a, p_b, v_a, v_b, t, beta, delta_h);
    t = t + d_t;
end
t_min_instant = t;
%fprintf('t_min_instant:\t\t%d\n\n', t_min_instant);


%% Banked Turn.

[p_a, p_b] = init_collision();
p_a_0 = p_a;
p_b_0 = p_b;

t_0 = 0;
h_0 = get_heading(v_b, beta);
run = true;
while run
    
    for bank = [-25:-1, 1:25]
        
        bank_rad = deg2rad(bank);
        
        p_a = p_a_0;
        p_b = p_b_0;
        
        i = 1;
        d_bank(i) = distance(p_a, p_b);
        i = i + 1;
        
        t = 0;
        %tStart = tic;
        while ~(abs(get_heading(p_b_0, p_b) - h_0) >= delta_h) %&& (toc(tStart) < 2)
            p_a_0 = p_a;
            p_b_0 = p_b;
            
            [d_bank(i), p_a, p_b] = bankedturn(p_a, p_b, v_a, v_b, beta_rad, bank_rad, t);
            
            t = t + d_t;
        end
        
        if min(d_bank) > d_sep
            t_min_banked = t;
            run = false;
            break;
        end
        
        d_bank = [];
    end
    
    t_0 = t_0 + d_t;
    [p_a_0, p_b_0] = move_bw(p_a_0, p_b_0, v_a, v_b, t, beta);
end
%fprintf('t_min_banked:\t\t%d\n\n', t_min_banked);

end