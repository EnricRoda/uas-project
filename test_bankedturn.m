close all; clear; clc;

%% Initialize variables

delta_h = 90; % deg

d_sep = 5; % NM

d_h = 5; % deg
d_t = 1; % s

beta = 180; % deg

v_a = 500; % ktsaxis equal
v_b = 170; % kts
t_c = 2;   % min

%% Convert units to IS

d_sep = convlength(d_sep, 'naut mi', 'm'); % m

v_a = convvel(v_a, 'kts', 'm/s'); % m/s
v_b = convvel(v_b, 'kts', 'm/s'); % m/s

%% TEST

beta_rad = 180*pi/180;

t_0 = 0;
h_0 = get_heading(v_b, beta);

figure; hold on; grid on;

for bank = [-25:-1, 1:25]
    
    bank_rad = bank*pi/180;
    
    [p_a, p_b] = init_position(v_a, v_b, t_c, beta);
    p_a_0 = p_a;
    p_b_0 = p_b;
    
    t = 0;
    %tStart = tic;
    while ~(abs(get_heading(p_b_0, p_b) - h_0) >= delta_h) %&& (toc(tStart) < 2)
        p_a_0 = p_a;
        p_b_0 = p_b;
        
        [~, p_a, p_b] = bankedturn(p_a, p_b, v_a, v_b, beta_rad, bank_rad, t);
        
        t = t + d_t;
        
        plot(p_b(1), p_b(2), 'k.');
    end
    
end

