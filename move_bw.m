function [p_a, p_b] = move_bw(p_a, p_b, v_a, v_b, t, beta, varargin)

% move_bw(p_a, p_b, v_a, v_b, t, beta)
% move_bw(p_a, p_b, v_a, v_b, t, beta, delta_h)

v_a = -v_a;
v_b = -v_b;

switch nargin - 6
    case 1
        delta_h = varargin{1};
        [p_a, p_b] = move_fw(p_a, p_b, v_a, v_b, t, beta, delta_h);
    otherwise
        [p_a, p_b] = move_fw(p_a, p_b, v_a, v_b, t, beta);
end

end