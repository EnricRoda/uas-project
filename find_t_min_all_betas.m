function [t_min_instant, t_min_banked, b] = find_t_min_all_betas(v_a, v_b, delta_h, d_sep)
%% Initialize variables

% v_a = 500; % kts
% v_b = 170; % kts
% delta_h = 90; % deg
% d_sep = 5; % NM


%% Find t_min for all betas

b = 0:359;
n = numel(b);

t_min_instant(n) = 0;
t_min_banked(n) = 0;

i = 1;
for beta = b
    [t_min_instant(i), t_min_banked(i)] = find_t_min(v_a, v_b, beta, delta_h, d_sep);
    i = i + 1;
end

end